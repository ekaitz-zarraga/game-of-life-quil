(ns game-of-life.visual
  (:require [quil.core :as q]
            [quil.middleware :as m]))

(defn setup! []
  (q/frame-rate 5)
  (q/background 200))

(defn draw-cell!
  ([[x y]]
   (draw-cell! [x y] 10))  ;; Default cell size 10px
  ([[x y] size]
   (q/fill 200)
   (q/fill 4)
   (q/rect (* size x) (* size y) size size)))

(defn draw!
  [cells]
  (q/background 255)
  (doall (map draw-cell! cells)))


(defn render-world
  [step init-cells]
  (q/defsketch game-of-life
    :title "Conway's Game of life"
    :settings #(q/smooth 2)
    :middleware [m/fun-mode]
    :setup (fn [] (setup!) init-cells)
    :update #(step %1)
    :draw draw!
    :size [400 400]))
