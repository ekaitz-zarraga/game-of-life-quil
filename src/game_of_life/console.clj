(ns game-of-life.console
  (:require [clojure.string :as string]))

; One print implementation there can be others
(defn render-world
  ([step init-cells] (render-world step init-cells 20))
  ([step init-cells size]
   (loop [cells init-cells]
     (print "\033[0;0H") ; Move cursor to beginning
     (print "\033[J")    ; Clear to end
     (let [r (range 0 (+ 1 size))]
       (println (string/join "\n" (for [y r] (apply str (for [x r] (if (cells [x y]) \# \ )))))))
     (Thread/sleep 250)
     (recur (step cells)))))
