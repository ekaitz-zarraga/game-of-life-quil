(ns game-of-life.core
  (:require
    ;[game-of-life.console :refer [render-world]]
    [game-of-life.visual :refer [render-world]]
    )
  (:gen-class))

(defn neighbours [[x y]]
  (for [dx [-1 0 1]
        dy (if (zero? dx) [-1 1] [-1 0 1])]
    [(+ dx x) (+ dy y)]))

(defn step [cells]
  (set (for [[loc n] (frequencies (mapcat neighbours cells))
             :when (or (= n 3) (and (= n 2) (cells loc)))]
         loc)))


; Some known structures
(def blinker    #{[1 2] [2 2] [3 2]})
(def glider     #{[1 0] [2 1] [0 2] [1 2] [2 2]})
(def pulsar     #{[9 5] [9 6] [9 7] [10 7] [14 7] [15 7] [15 6] [15 5] [7 9]
                  [7 10] [6 9] [5 9] [17 9] [19 9] [18 9] [17 10] [7 14] [7 15]
                  [6 15] [5 15] [17 14] [17 15] [18 15] [19 15] [15 17] [9 17]
                  [10 17] [9 18] [9 19] [14 17] [15 18] [15 19] [10 9] [11 9]
                  [11 10] [14 9] [13 9] [13 10] [14 15] [13 15] [13 14] [10 15]
                  [11 15] [11 14] [9 14] [9 13] [10 13] [14 13] [15 13] [15 14]
                  [14 11] [15 10] [15 11] [10 11] [9 10] [9 11]})

; Declare some main, pal
(defn -main [& args]
  (render-world step pulsar))
