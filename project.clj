(defproject game_of_life "0.1.0-SNAPSHOT"
  :description "Conway's Game of Life with Quil and Console"
  :url ""
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [quil "3.0.0"]]
  :repl-options {:init-ns game-of-life.core}
  :main game-of-life.core
  :aot [game-of-life.core]
  )
